#!/usr/bin/env bash

cd ~/timilink-sync-file

FILE=nginx/sync_file
if [ ! -f "$FILE" ]; then
    echo "$FILE does not exist."
    exit
fi

cp nginx/sync_file /etc/nginx/sites-available/
ln -s /etc/nginx/sites-available/sync_file /etc/nginx/sites-enabled/sync_file
service nginx restart

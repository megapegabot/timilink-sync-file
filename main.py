import aiohttp
import asyncio
import logging
import os
import argparse
from asyncio import sleep, ensure_future
from aiohttp.web_exceptions import HTTPException

log = logging.getLogger(__name__)

def init(media_dir):
    if not os.path.exists(media_dir):
        os.mkdir(media_dir)

async def download(session, url, target):
    async with session.get(url) as response:
        with open(target, 'wb') as f_handle:
            while True:
                chunk = await response.content.read(8 * 1024)
                if not chunk:
                    break
                f_handle.write(chunk)
        await response.release()


async def reset_app_nodm():
    os.system('killall -9 /usr/sbin/nodm')


class SYNC:
    def __init__(self, session, media_dir, server, term_id):
        self._session = session
        self._media_dir = media_dir
        self._term_id = term_id
        self._server = server
        self._dpool = []
        self._lpool = []
        self._video_sync_task = ensure_future(self.video_sync())
        self._data = {}

    async def video_sync(self):
        while True:
            try:
                await self._fetch()
            except Exception:
                log.exception('Data sync error: ')

            try:
                await self._download_video()
            except Exception:
                log.exception('Download error: ')

            try:
                await self._check_file()
            except Exception:
                log.exception('Check_file: ')

            await sleep(30)

    async def _check_file(self):
        if not self._lpool:
            self._lpool = self._dpool
            return
        if self._dpool != self._lpool:
            for video in self._lpool:
                if not video in self._dpool:
                    os.remove(os.path.join(self._media_dir, video))
            self._lpool = self._dpool.copy()
            self._dpool.clear()
            # Если были изменения роликов
            log.info('Были изменения роликов, делаю рестат nodm')
            await reset_app_nodm()

    async def _fetch(self):
        async with self._session.get(f'{self._server}/brs/select/{self._term_id}') as resp:
            self._data = await resp.json()
            return self._data

    async def _download_video(self):
        for video in self._data:
            filename = os.path.basename(video['content']['hash'])
            ext = video['content']['ext']
            link_download = self._server + video['content']['url']
            target = os.path.join(self._media_dir, filename + ext)
            self._dpool.append(target)
            if not os.path.exists(target):
                try:
                    await download(self._session, link_download, target)
                except HTTPException as exc:
                    log.error('Download (%s) failed: %r', video['url'], exc)


async def main(loop, args):
    async with aiohttp.ClientSession() as session:
        sync = SYNC(session=session,
                    server= args.server,
                    media_dir=args.media_dir,
                    term_id=args.term_id,
                    )
        while True:
            await sleep(1)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--server', default='http://ostanovki-touch.ekadm.ru')
    parser.add_argument('--media_dir', default='./video')
    parser.add_argument('--term_id', type=str, default= '9')
    args = parser.parse_args()

    logging.getLogger().setLevel(logging.INFO)
    logging.basicConfig(filename='log.txt', format='%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    loop = asyncio.get_event_loop()
    log.info('Started')
    init(args.media_dir)
    loop.run_until_complete(main(loop, args))
